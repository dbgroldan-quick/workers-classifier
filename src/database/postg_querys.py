import psycopg2
from utils.filter_generators import generateSQLFilter
from utils.validation_data import validation_json_types

class ObjectDAO:
    def __init__(self, connection, table_name):
        self.conn, self.cursor =  connection.getConnection()
        self.table_name = table_name
        self.params = self.defineParams()

    def defineParams(self):
        table = self.table_name.split('.')[-1]
        query = """SELECT column_name
            FROM information_schema.columns
            WHERE table_name = '{}';
            """.format(table)
        params = []
        try:
            self.cursor.execute(query)
            objs_query = self.cursor.fetchall()
            for elem in objs_query:
                params.append(elem[0])
        except (Exception, psycopg2.Error) as error:
            print("TB Error:", error)
        return params

    def findByFilter(self, filt):
        separator_SQL = list(filt.keys())
        query = 'SELECT * FROM {} WHERE '.format(self.table_name)
        query += generateSQLFilter(filt, separator_SQL)
        query += ';'
        objs = []
        
        try:
            self.cursor.execute(query)
            objs_query = self.cursor.fetchall()
            for elem in objs_query:
                res = {self.params[i] : elem[i] for i, _ in enumerate(elem)}
                res = validation_json_types(res)
                objs.append(res)
        except (Exception, psycopg2.Error) as error:
            print("TB Error:", error)
        return objs

    def countData(self, filt=None):
        query = 'SELECT COUNT(*) FROM {}'.format(self.table_name)
        if filt:
            separator_SQL = list(filt.keys())
            query += ' WHERE {}'.format(generateSQLFilter(filt, separator_SQL))
        query += ';'
        obj = None
        
        try:
            self.cursor.execute(query)
            obj = self.cursor.fetchone()[0]
        except (Exception, psycopg2.Error) as error:
            print("TB Error:", error)
        return obj
    
    def findAll(self):
        objs = []
        query = 'SELECT * FROM {};'.format(self.table_name)
        try:
            self.cursor.execute(query)
            objs_query = self.cursor.fetchall()
            for elem in objs_query:
                res = {self.params[i] : elem[i] for i, _ in enumerate(elem)}
                res = validation_json_types(res)
                objs.append(res)
        except (Exception, psycopg2.Error) as error:
            print("TB Error:", error)
        return objs
