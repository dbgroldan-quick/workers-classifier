import datetime
from config.settings import get_config
from database.postg_querys import ObjectDAO
from database.postg_connection import Connection
from utils.query_tools import execute_query, get_specific_queries

config = get_config()
db_data = config.get('db_config')
DB_CONFIG = 'dbname=%s user=%s host=%s port=%s password=%s'%db_data

DEFAULT_LEVEL = config.get('default_level')
SECOND_LEVEL = config.get('second_level')
THIRD_LEVEL = config.get('third_level')

JACKET, SUITCASE = config.get('work_tools')
NUM_MONTHS = config.get('num_months')
NUM_MIN_SERVICES = config.get('num_min_services')
BAG, CREDIT_CARD, PSE, CREDIT = config.get('payment_methods')

MENSAJERIA, DOMICILIO = config.get('service_types')

class ServicesClassifier:
    def __init__(self):
        self.conn = Connection(DB_CONFIG)
        self.services = ObjectDAO(self.conn, 'services')
        self.workers = ObjectDAO(self.conn, 'workers')
        self.balances = ObjectDAO(self.conn, 'balances')

    def get_services(self, worker_id):
        level = self.define_level(worker_id)
        if isinstance(level, str):
            return 200, {'message': level}

        filter_first = {
            'and':[
                {
                    'condition': '=',
                    'param': 'service_type_id',
                    'value': MENSAJERIA

                }
            ],
            'or':[ 
                    {
                        'condition': '=',
                        'param': 'payment_method_id',
                        'value': CREDIT_CARD
                    },
                    {
                        'condition': '=',
                        'param': 'payment_method_id',
                        'value': PSE
                    },
                    {
                        'condition': '=',
                        'param': 'payment_method_id',
                        'value': CREDIT
                    }
                ]
            }
        if level == SECOND_LEVEL or level == DEFAULT_LEVEL:
            if level == SECOND_LEVEL:
                filter_cash = {
                    'condition': '=',
                    'param': 'payment_method_id',
                    'value': BAG
                }
                filter_express = {
                    'condition': '=',
                    'param': 'service_type_id',
                    'value': MENSAJERIA

                }
                filter_delivery = {
                    'condition': '=',
                    'param': 'service_type_id',
                    'value': DOMICILIO

                }
                del filter_first['and']
                # Balance verification
                balance = self.define_price_balance(worker_id)
                if balance:
                    filter_price = {
                        'condition': '>',
                        'param': 'declared_value',
                        'value': self.define_price_balance(worker_id)
                    }                
                    filter_first['and'] = [ filter_price ] 
                else:
                    return 404, {
                        'message': 'User without balance'
                    }
                filter_first['or'].append(filter_cash)
                filter_first['or'].append(filter_express)
                filter_first['or'].append(filter_delivery)
            return 200, {
                'worker_level': level,
                'services': self.services.findByFilter(filter_first)
                }

        return 200, {'worker_level': level, 'services': self.services.findAll()}

    def define_price_balance(self, worker_id):
        user = self.worker.findByFilter({
                    'condition': '=',
                    'param': 'worker_id',
                    'value': worker_id
                })
        balance = self.balances.findByFilter({
                    'condition': '=',
                    'param': 'user-id',
                    'value': user[-1].get('users_id')
                })
        return balance[-1].get('value')

    def define_level(self, worker_id):
        level = DEFAULT_LEVEL
        filt = {
                'and': [
                    {
                        'condition': '=',
                        'param': 'worker_id',
                        'value': worker_id
                    }
                    ]
                }
        ## Second level assessment
        count_services = 0 if not self.services.countData(filt) else self.services.countData(filt)
        if count_services < NUM_MIN_SERVICES:
            filt['and'][0]['param'] = 'id'
            worker = self.workers.findByFilter(filt)

            if not worker:
                return "Worker not found"

            worker = worker[-1]
            if worker.get('active'):
                now = datetime.datetime.now(datetime.timezone.utc)
                if abs((worker['created_at'] - now).days / 30) > NUM_MONTHS:
                    params_jacket = {
                        'worker_id': worker_id,
                        'tool_id': JACKET
                    }
                    query_jacket = get_specific_queries('verify_worker_tools',
                            {'worker_id': worker_id, 'tool_id': JACKET })  
                    query_suitcase = get_specific_queries('verify_worker_tools',
                            {'worker_id': worker_id, 'tool_id': SUITCASE })
                    if execute_query(self.conn, query_jacket) \
                        or execute_query(self.conn, query_suitcase):
                        level = SECOND_LEVEL
                        ## Third level assessment
                        if execute_query(self.conn, query_jacket)\
                             and execute_query(self.conn, query_suitcase):
                            ### Insurance number verfication
                            query_insurance = get_specific_queries('verify_insurance_number',
                            {'worker_id': worker_id }) 
                            if execute_query(self.conn, query_insurance):                                
                                level = THIRD_LEVEL
            else:
                return "Worker is not active"
        return int(level)