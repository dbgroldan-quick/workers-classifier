import os
from flask import Blueprint, jsonify, request, make_response
from werkzeug.utils import secure_filename
from services_classifier.classifier_service import ServicesClassifier
from utils.validation_data import buildResponse, errorContent

content_type = 'application/json'

def cons_classif_blueprint(secret):
    classifier_bp = Blueprint('classifier_bp', __name__)
    services_control = ServicesClassifier()

    @classifier_bp.route('/classifier/<worker_id>', methods = ['GET'])
    def classyfy_services(worker_id):
        if request.content_type == content_type:
            content = services_control.get_services(worker_id)
        else:
            content = errorContent()
        return buildResponse(content_type, content)
    return classifier_bp
