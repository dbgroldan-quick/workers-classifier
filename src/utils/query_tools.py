import psycopg2
from config.settings import get_config

config = get_config()
# Default_vehicle
MOTO = config.get('moto')

def execute_query(connection, query):
    _, cursor = connection.getConnection()
    obj = None
    try:
        cursor.execute(query)
        obj = cursor.fetchall()
    except (Exception, psycopg2.Error) as error:
        print("TB Error:", error)
    return obj


def get_specific_queries(option, params):
    worker_id = params.get('worker_id')
    tool_id = params.get('tool_id')
    vehicle_type = params.get('vehicle_type')
    vehicle_type = MOTO if not vehicle_type else vehicle_type

    if option == 'verify_worker_tools':        
        if worker_id and tool_id: 
            return """ SELECT * FROM service_tools_type as serv_tools
                    LEFT JOIN tools_types as tools
                    ON serv_tools.servicetoolstype_id = tools.id
                    LEFT JOIN services
                    ON services.id = serv_tools.service_id
                    LEFT JOIN vehicles
                    ON vehicles.worker_id = services.worker_id
                    WHERE services.worker_id = {} AND tools.id = {} 
                    AND vehicles.vehicle_type_id={};
                    """.format(worker_id, tool_id, vehicle_type)
    if option == 'verify_insurance_number':
        if worker_id:
            return """
                SELECT bond.insurance_number FROM bonding_worker AS bond
                LEFT JOIN vehicles
                ON bond.car_plate = vehicles.car_plate
                LEFT JOIN workers w on vehicles.worker_id = w.id
                WHERE w.id = {} AND vehicles.vehicle_type_id = {} 
                AND bond.insurance_number is not null;
                """.format(worker_id, vehicle_type)
    return None