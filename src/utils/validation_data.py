from flask import make_response, jsonify
import decimal

# Build Response with content-type
def buildResponse(content_type, content):
    code, message = content
    resp = make_response(jsonify(message), code)
    resp.headers['Content-Type'] = content_type
    return resp

# Error Message in content-type
def errorContent():
    return 403, {"error": "Request should have 'Content-Type' header with value 'application/json'"}

def validation_json_types(dict_rev):
    for key in list(dict_rev.keys()):
        dict_rev[key] = (float(dict_rev[key]) 
        if isinstance(dict_rev[key], decimal.Decimal) else dict_rev[key])
    return dict_rev