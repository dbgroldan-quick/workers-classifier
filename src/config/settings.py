import os
from dotenv import load_dotenv
from pathlib import Path

env_path = Path('.') / '.env'
load_dotenv()

def get_config():
    return {
        'db_config': (os.getenv('DB_NAME'), os.getenv('DB_USER'),
            os.getenv('DB_HOST'), os.getenv('DB_PORT'),
            os.getenv('DB_PASSWORD')),
        'port_http': os.getenv("PORT_HTTP"),
        'host': os.getenv("HOST"),

        'default_level': os.getenv("DEFAULT_LEVEL"),
        'second_level': os.getenv("SECOND_LEVEL"),
        'num_min_services': int(os.getenv("NUM_MIN_SERVICES")),
        'num_months': int(os.getenv("NUM_MIN_MONTHS")),

        'third_level': os.getenv("THIRD_LEVEL"),

        'service_types': (os.getenv("MENSAJERIA"), os.getenv("DOMICILIO")),
        
        'work_tools': (os.getenv("JACKET"), os.getenv("SUITCASE")),
        'payment_methods': (os.getenv("BAG"), os.getenv("CREDIT_CARD"),
         os.getenv("PSE"), os.getenv("CREDIT")),

        'moto': os.getenv("MOTO"),
    }