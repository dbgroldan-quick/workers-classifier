from flask import Flask
from services_classifier.classifier_routes import cons_classif_blueprint
from config.settings import get_config
from utils.validation_data import buildResponse

# Config Server
config = get_config()
port = config.get('port_http')
host = config.get('host')

# Server Application
app = Flask(__name__)
server_key = 'developedbyquickapp'
app.secret_key = server_key

# Routes
app.register_blueprint(cons_classif_blueprint(server_key), url_prefix='')

# Error Routes
@app.errorhandler(404)
def not_found(e):
    content = 200, {'error': 'Not found'}
    return buildResponse('application/json', content)

# Application initialization
if __name__ == '__main__':
    print('Server is listening in port: ' + str(port))
    app.run(port=port, debug=True, host = host)