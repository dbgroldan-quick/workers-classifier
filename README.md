# Workers Classifier
### Use
This is a sertvice than with a each request, show services according worker classification.


### How to use this project

1. **Clone this Repo**
  ```sh
  → git clone
  ```

2. **If there were no errors in the previous step**
  ```sh
  → docker-compose up --build
  ```

3. **Enter the web page, on the routes:**

[127.0.0.1:4000](http://127.0.0.1:4000 "Link a Pagina")

[localhost:4000](http://localhost:4000 "Link a Pagina")


### License
MIT License